# Innovecs test task

This is [repository](https://gitlab.com/kovalexeyko/innovecs-test.git "UI-tests-Innovecs") inline link.

Project clone url: https://gitlab.com/kovalexeyko/innovecs-test.git

# Params:
   * suiteFile - for chose suite xml
   * browser - chrome or firefox. Default is chrome
   * local - "true" for running tests on local machine. "false" - for running tests with grid. Default is true

   
# Example: 
    mvn -DsuiteFile=test.xml -Dbrowser=chrome -Dlocal=false clean test

    
# Reporting: 
    mvn allure:serve    

# Test cases
[Link to test cases for this project](test-cases-followers.txt)
