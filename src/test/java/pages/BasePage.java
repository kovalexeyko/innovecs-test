package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.loader.HtmlElementLoader;

import java.util.List;

public class BasePage {

    public WebDriver driver;

    @FindBy(xpath = ".//*[@routerlink='/login']")
    public WebElement login;

    @FindBy(xpath = "//*[contains(text(), 'Home') and contains(@class,'nav-link')]")
    public WebElement homePage;

    @FindBy(xpath = "//*[contains(text(), 'Global Feed') and contains(@class,'nav-link')]")
    public WebElement globalFeed;

    List<ArticleListRow> articles;

    public BasePage(WebDriver driver) {
        HtmlElementLoader.populatePageObject(this, driver);
        this.driver = driver;
    }

    @Step
    public void goToLoginPage() {
        login.click();
    }

    @Step
    public void goToHomePage() {
        homePage.click();
    }

    @Step
    public void openArticleByNumber(int articleNumber) {

        articles.get(articleNumber).article.click();
    }

    @Step
    public void openProfileFromArticleByNumber(int articleNumber) {

        articles.get(articleNumber).author.click();
    }


    public void waitForElement(By xPath) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(xPath));
    }

    public void timeout(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
