package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

@FindBy(xpath = ".//app-article-list-item")
public class ArticleListRow extends HtmlElement {
    @FindBy(xpath = ".//a[@class='author']")
    public WebElement author;

    @FindBy(xpath = ".//a[@class='preview-link']")
    public WebElement article;

    public String getAuthor() {
        return author.getText();
    }
}
