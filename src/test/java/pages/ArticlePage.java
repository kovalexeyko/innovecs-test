package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.loader.HtmlElementLoader;

import java.util.List;

public class ArticlePage extends BasePage {

    @FindBy(xpath = ".//div[@class='container']//button[contains(@class,'action-btn')]")
    public WebElement followButtonHeader;

    @FindBy(xpath = ".//div[@class='container page']//button[contains(@class,'action-btn')]")
    public WebElement followButtonBottom;

    @FindBy(xpath = ".//a[@class='author']")
    public WebElement author;

    public ArticlePage(WebDriver driver) {
        super(driver);
        HtmlElementLoader.populatePageObject(this, driver);
    }

    @Step
    public void clickFollowButton() {
        followButtonHeader.click();
    }

    public String getFollowerName() {
        timeout(1);
        return author.getText();
    }
}
