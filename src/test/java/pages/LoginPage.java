package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

    @FindBy(xpath = ".//*[@placeholder='Username']")// better to use some id and add it to element
    private WebElement userName;

    @FindBy(xpath = ".//*[@placeholder='Password']")
    private WebElement passwordInput;

    @FindBy(xpath = ".//button")
    private WebElement signInButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @Step()
    public void login(String mail, String password) {
        clearAll();
        userName.sendKeys(mail);

        passwordInput.sendKeys(password);

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        signInButton.click();
    }

    private void clearAll() {
        userName.clear();
        passwordInput.clear();
    }
}
