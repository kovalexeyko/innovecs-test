package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.loader.HtmlElementLoader;

public class SettingsPage extends BasePage {

    @FindBy(xpath = ".//button[@class='btn btn-outline-danger']")
    private WebElement logoutButton;

    public SettingsPage(WebDriver driver) {
        super(driver);
        HtmlElementLoader.populatePageObject(this, driver);
    }

    @Step
    public void logout() {
        logoutButton.click();
    }
}
