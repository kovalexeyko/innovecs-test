package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.loader.HtmlElementLoader;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LoggedInPage  extends BasePage {

    @FindBy(xpath = "//*[contains(text(), 'Your Feed') and contains(@class,'nav-link')]")
    private WebElement yourFeed;

    @FindBy(xpath = "//*[contains(text(), 'Global Feed') and contains(@class,'nav-link')]")
    private WebElement globalFeed;

    @FindBy(xpath = "(//div[@class='app-article-preview'])[2]")
    private WebElement appArticlePreview;

    @FindBy(xpath = "//a[@class='page-link']")
    private List<WebElement> pagesLinks;

    @FindBy(xpath = ".//*[@routerlink='/settings']")
    private WebElement settingsPage;

    List<ArticleListRow> articles;

    public LoggedInPage(WebDriver driver) {
        super(driver);
        HtmlElementLoader.populatePageObject(this, driver);
    }

    @Step
    public void openYourFeedTab() {
        yourFeed.click();

        waitForArticles();
    }

    @Step
    public void openGlobalFeedTab() {
        waitForElement(By.xpath(".//*[@class='banner']//*[@class='logo-font']"));

        globalFeed.click();

        waitForArticles();
    }

    @Step
    public void waitForArticles() {
        waitForElement(By.xpath(".//app-article-list-item"));
    }

    @Step
    public boolean isArticlesPresent() {
        timeout(1);

        if(appArticlePreview.getText().equals("No articles are here... yet."))
            return false;
        else
            return true;
    }

    @Step
    public Set<String> getAuthors() {

        Set<String> set = new HashSet<>();
        if(pagesLinks==null || pagesLinks.size()==1) {
            articles.forEach(article -> {
                set.add(article.getAuthor());
            });
        } else {
            pagesLinks.forEach(number->{
                number.click();
                waitForArticles();
                articles.forEach(article -> {
                    set.add(article.getAuthor());
                });

            });
        }

        return set;
    }

    @Step
    public void goToSettingsPage() {
        settingsPage.click();
    }
}
