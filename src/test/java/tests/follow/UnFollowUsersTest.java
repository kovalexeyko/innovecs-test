package tests.follow;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.BaseTest;

import java.util.Set;


public class UnFollowUsersTest extends BaseTest {

    @BeforeMethod
    public void login(){
        basePage.goToLoginPage();
        loginPage.login("testtest@test.test", "test");
        loggedInPage.openGlobalFeedTab();
        loggedInPage.openArticleByNumber(0);

        String follower = articlePage.getFollowerName();

        System.out.println("follower="+follower);

        articlePage.clickFollowButton();
    }

    @Test(priority = 1)
    public void unFollowInArticlePage() throws InterruptedException {
        loggedInPage.goToHomePage();

        loggedInPage.openYourFeedTab();
        loggedInPage.openArticleByNumber(0);
        articlePage.clickFollowButton();
        articlePage.goToHomePage();

        boolean isArticlesPresent = loggedInPage.isArticlesPresent();

        System.out.println("isArticlesPresent="+isArticlesPresent);
        Assert.assertFalse(isArticlesPresent, "Articles for follower are still present");
    }

    @Test(priority = 2)
    public void unFollowInProfilePage() {
        loggedInPage.goToHomePage();

        loggedInPage.openYourFeedTab();
        loggedInPage.openProfileFromArticleByNumber(0);
        profilePage.clickFollowButton();
        profilePage.goToHomePage();

        boolean isArticlesPresent = loggedInPage.isArticlesPresent();

        System.out.println("isArticlesPresent="+isArticlesPresent);
        Assert.assertFalse(isArticlesPresent, "Articles for follower are still present");
    }

    @AfterMethod
    public void logout(){
        loggedInPage.goToSettingsPage();
        settingsPage.logout();
    }
}
