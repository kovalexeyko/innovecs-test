package tests.follow;

import org.testng.Assert;
import org.testng.annotations.*;
import tests.BaseTest;

import java.util.Set;


public class FollowUsersTest extends BaseTest {

    @BeforeMethod
    public void login(){
        basePage.goToLoginPage();
        loginPage.login("testtest@test.test", "test");
    }

    @Test(priority = 1)
    public void addFollowerFromArticle() throws InterruptedException {
        loggedInPage.openGlobalFeedTab();
        loggedInPage.openArticleByNumber(0);

        String follower = articlePage.getFollowerName();

        System.out.println("follower="+follower);

        articlePage.clickFollowButton();
        articlePage.goToHomePage();
        loggedInPage.openYourFeedTab();
        loggedInPage.waitForArticles();
        Set<String> authors = loggedInPage.getAuthors();
        System.out.println("authors="+authors);

        Assert.assertTrue(authors.contains(follower), "Author " + follower + " not found in followers");
    }

    @Test(priority = 2)
    public void addTwoFollowersFromArticle() {
        // first follower
        loggedInPage.openGlobalFeedTab();
        loggedInPage.openArticleByNumber(0);

        String follower = articlePage.getFollowerName();

        System.out.println("follower="+follower);
        articlePage.clickFollowButton();
        articlePage.goToHomePage();

        // second follower
        loggedInPage.openGlobalFeedTab();
        loggedInPage.openArticleByNumber(9);

        String followerSecond = articlePage.getFollowerName();

        System.out.println("followerSecond="+followerSecond);
        articlePage.clickFollowButton();
        articlePage.goToHomePage();

        loggedInPage.openYourFeedTab();
        loggedInPage.waitForArticles();
        Set<String> authors = loggedInPage.getAuthors();
        System.out.println("authors="+authors);

        Assert.assertTrue(authors.contains(follower), "Author " + follower + " not found in followers");
        Assert.assertTrue(authors.contains(followerSecond), "Author " + followerSecond + " not found in followers");
    }

    @Test(priority = 3)
    public void addFollowerFromProfile() throws InterruptedException {
        loggedInPage.openGlobalFeedTab();
        loggedInPage.openProfileFromArticleByNumber(0);

        String follower = profilePage.getFollowerName();

        System.out.println("follower="+follower);

        profilePage.clickFollowButton();
        profilePage.goToHomePage();
        loggedInPage.openYourFeedTab();
        loggedInPage.waitForArticles();
        Set<String> authors = loggedInPage.getAuthors();
        System.out.println("authors="+authors);

        Assert.assertTrue(authors.contains(follower), "Author " + follower + " not found in followers");
    }

    @AfterMethod
    public void unfollowAllUsers(){
        loggedInPage.goToHomePage();
        boolean isArticlesPresent;
        do {
            loggedInPage.openYourFeedTab();
            loggedInPage.openArticleByNumber(0);
            articlePage.clickFollowButton();
            articlePage.goToHomePage();
            isArticlesPresent = loggedInPage.isArticlesPresent();
        } while (isArticlesPresent);

        loggedInPage.goToSettingsPage();
        settingsPage.logout();
    }
}
