package tests;

import helpers.BrowserDriver;
import helpers.GridUtils;
import helpers.LocalBrowserUtils;
import helpers.PropertiesReader;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import pages.*;

import java.io.IOException;

import static helpers.BrowserDriver.CHROME;

public class BaseTest {

    public WebDriver driver;
    public GridUtils gridUtils = new GridUtils();
    public String APP_URL = "qa-task.backbasecloud.com";
    public String user = "candidatex";
    public String password = "qa-is-cool";

    public String GRID_URL = "http://10.65.150.55:4444/wd/hub";
    public BasePage basePage;
    public LoginPage loginPage;
    public LoggedInPage loggedInPage;
    public ArticlePage articlePage;
    public SettingsPage settingsPage;
    public ProfilePage profilePage;
    private PropertiesReader reader;

    @BeforeTest
    public void setUpDriver() throws IOException {

        // Get the type of browser
        reader = new PropertiesReader("run.properties");
        String property = reader.getProperty("browser");

        BrowserDriver browser = BrowserDriver.valueOf(property.toUpperCase());
        boolean ifLocal = Boolean.parseBoolean(reader.getProperty("local"));

        if (ifLocal) {
            switch (browser) {
                case CHROME:
                    driver = LocalBrowserUtils.getLocalChromeDriver();
                    break;
                case FIREFOX:
                    driver = LocalBrowserUtils.getLocalFirefoxDriver();
                    break;
            }
        } else {
            switch (CHROME) {
                case CHROME:
                    driver = gridUtils.getChromeDriver(GRID_URL);
                    break;
                case FIREFOX:
                    driver = gridUtils.getFirefoxDriver(GRID_URL);
                    break;
            }
        }

        driver.get("https://" + user + ":" + password + "@" + APP_URL);
    }

    @BeforeClass
    public void setupPages() {
        basePage = new BasePage(driver);
        loginPage = new LoginPage(driver);
        loggedInPage = new LoggedInPage(driver);
        articlePage = new ArticlePage(driver);
        settingsPage = new SettingsPage(driver);
        profilePage = new ProfilePage(driver);
    }

    @AfterTest
    public void closeDriver() {

        driver.quit();
    }

}


