package helpers;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class GridUtils {
    private WebDriver driver;

    public WebDriver getChromeDriver(String hubUrl) {
        return getChromeDriver(hubUrl, "UiIdeals");
    }

    public WebDriver getChromeDriver(String hubUrl, String useragent) {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setAcceptInsecureCerts(true);
        //connect to grid
        if(useragent != null) {
            chromeOptions.addArguments(useragent);
        }
        return driverRegister(chromeOptions, hubUrl);
    }

    public WebDriver getPhantomJsDriver(String hubUrl) {
        //connect to grid
        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setBrowserName("phantomjs");
        capability.setPlatform(Platform.ANY);
        return driverRegister(capability, hubUrl);
    }

    public WebDriver getFirefoxDriver(String hubUrl) {
        //connect to grid
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        return driverRegister(firefoxOptions, hubUrl);
    }

    public WebDriver getHeadlessFirefoxDriver(String hubUrl, Dimension dimension) {
        //connect to grid
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setHeadless(true);
        firefoxOptions.setAcceptInsecureCerts(true);
        try {
            driver = new RemoteWebDriver(new URL(hubUrl), firefoxOptions);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.manage().window().setSize(dimension);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        return driver;
    }

    public WebDriver getHeadlessChromeDriver(String hubUrl, Dimension dimension) {
        //connect to grid
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setHeadless(true);
        chromeOptions.setAcceptInsecureCerts(true);
        try {
            driver = new RemoteWebDriver(new URL(hubUrl), chromeOptions);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.manage().window().setSize(dimension);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        return driver;
    }

    private WebDriver driverRegister(Capabilities capabilities, String hubUrl) {
        try {
            driver = new RemoteWebDriver(new URL(hubUrl), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        //implicity wait
        driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        return driver;
    }
}
