package helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class LocalBrowserUtils {
    private static String pathToPhantomjsDriver, pathToGeckoDriver, pathToChromedriver;


    public static WebDriver getLocalFirefoxDriver() {
        setDriverPath();
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("acceptInsecureCerts", true);
        firefoxOptions.merge(capabilities);
        WebDriver driver = new FirefoxDriver(firefoxOptions);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver getLocalChromeDriver() {
        setDriverPath();
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.BROWSER, Level.ALL);
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setAcceptInsecureCerts(true);
//        chromeOptions.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
        WebDriver driver = new ChromeDriver(chromeOptions);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver;
    }

//    public static WebDriver getLocalPhantomJsDriver() {
//        setDriverPath();
//
//        DesiredCapabilities dcaps = new DesiredCapabilities();
//        dcaps.setCapability("takesScreenshot", true);
//        dcaps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[]{"--web-security=no", "--ignore-ssl-errors=yes"});
//        WebDriver driver = new PhantomJSDriver(dcaps);
//        driver.manage().window().setSize(new Dimension(1600, 1200));
//        driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
//        return driver;
//    }

    private static String checkOS() {
        return System.getProperty("os.name");
    }

    private static void setDriverPath() {
        if (checkOS().contains("Windows")) {
            pathToPhantomjsDriver = "drivers/phantomjs.exe";
            pathToChromedriver = "drivers/chromedriver.exe";
            pathToGeckoDriver = "drivers/geckodriver.exe";
        } else {
            pathToPhantomjsDriver = "drivers/phantomjs";
            pathToChromedriver = "drivers/chromedriver";
            pathToGeckoDriver = "drivers/geckodriver";
        }

        System.setProperty("webdriver.gecko.driver", pathToGeckoDriver);
        System.setProperty("phantomjs.binary.path", pathToPhantomjsDriver);
        System.setProperty("webdriver.chrome.driver", pathToChromedriver);
    }
}
