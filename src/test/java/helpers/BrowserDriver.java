package helpers;

public enum BrowserDriver {

    CHROME("chrome"),
    FIREFOX("firefox");

    private String browser;

    BrowserDriver(String browser) {
        this.browser = browser;
    }

    public String getBrowser() {
        return browser;
    }
}
